# even, odd and prime numbers in a given list

def seperate_numbers(input_list):
    even_num = []
    odd_num = []
    prime_num = []
    
    for num in input_list:
        if num % 2 == 0:     #checking even number
            even_num.append(num)
            if num == 2:                # even numbers cant be prime except '2'
                prime_num.append(num)
        else:
            odd_num.append(num)
            count = True
            for i in range(3, num, 2):
                if num%i == 0:
                    count = False
            if count:
                prime_num.append(num)
    return even_num, odd_num, prime_num

input_list = [1,2,3,4,5,6,7,8]
even, odd, prime = seperate_numbers(input_list)
print(f"even_numbers : {even}")
print(f"odd_numbers : {odd}")
print(f"prime_numbers : {prime}")
    