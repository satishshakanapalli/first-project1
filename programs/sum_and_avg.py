# sum of number in a given list

def sum_of_numbers(input_list):
    total_sum = 0
    for num in input_list:
        total_sum += num
    return total_sum

input_list = [1,2,3,4,5,6,7,8]
total_sum = sum_of_numbers(input_list)
print(f"sum of numbers in a given list : {total_sum}")


# average of given input list

def avg_of_input_list(input_list, len_of_input_list):
    avg = sum_of_numbers(input_list) / len_of_input_list
    return avg

len_of_input_list = len(input_list)
average = avg_of_input_list(input_list, len_of_input_list)
print(f"average of given list : {average}")